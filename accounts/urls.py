
from django.urls import path
from accounts import views

urlpatterns = [
    
    path('home', views.home , name= "home"),
    path('productos', views.productos , name="productos"),
    path('customer' , views.customer , name= "customer"),
    path('crearOrden', views.crearOrden, name ="CrearOrden"),
    path('crearCustomer', views.crearCustomer, name ="CrearCustomer"),
    path('eliminarOrden/<int:pk>/', views.eliminarOrden, name = "eliminarOrden"),
    path('updateCustomer/<int:id>/', views.updateCostumer, name="updateCostumer"),
    path('updateOrden/<int:id>/', views.updateOrden , name="updateOrden"),
    path('signin', views.signin , name= "signin"),
    path('register', views.register, name = "register"),
    path('logout', views.logoutUser, name ="logout"),
    path('crud/', views.crud, name ="crud"),
    path('crear_producto/', views.crear_producto, name="crear_producto"),
    path('boton/', views.boton, name="boton"),
    
    
]
