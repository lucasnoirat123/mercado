import django_filters
from django_filters import filters
from .models import *


class OrdenFilter(django_filters.FilterSet):
    class Meta:
        model = Orden
        fields = ['customer','producto','status']
