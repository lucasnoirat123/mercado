

from typing import ContextManager
from django.db.models import query
from django.http import JsonResponse
from django.shortcuts import redirect, render
from django.http import HttpResponse
from .models import Customer , Orden, Producto
from .forms import CustomerForm, OrdenForm, ProductoForm, CreateUserForm
from .filters import OrdenFilter
from django.contrib import messages
from django.contrib.auth import authenticate , login , logout
from django.contrib.auth.decorators import login_required  
from django.contrib.auth import get_user_model
import json
from django.views.generic import View
# Create your views here.
from django.core import serializers
from time import time


@login_required(login_url ='signin')
def home(request):

    customers = Customer.objects.all()
    ordenes  = Orden.objects.all()

    total_ordenes = Orden.objects.count()
    delivered  = Orden.objects.filter(status = 'progreso').count()
    pendiente  = Orden.objects.filter(status = 'pendiente').count()
    username = None
    if request.user.is_authenticated:
        username = request.user.username
        User = get_user_model()
        users = User.objects.all()
        
   
    context = {
    'ordenes':ordenes ,
    'customers':customers,
    'total_ordenes':total_ordenes,
    'delivered':delivered,
    'pendiente':pendiente,
    'username':username,
    'users':users
    
     }

    

    return render(request , 'accounts/home.html', context)

@login_required(login_url ='signin')
def productos(request):
    form = ProductoForm()
    if request.method == 'POST':
            form = ProductoForm(request.POST)
            if form.is_valid:
                form.save()


    productos = Producto.objects.all()
    context = {'productos':productos,
                'form':form           }

    return render(request , 'accounts/productos.html',context)





@login_required(login_url ='signin')
def customer(request):

    #customers = Customer.objects.get(id= id)
    orden  = Orden.objects.all().order_by('customer')
    form = OrdenForm()
    total_ordenes = Orden.objects.count()
    filter = OrdenFilter(request.GET, queryset= orden)
    if request.method == 'GET':
        orden = filter.qs


    context = {
        'form':form,
        'orden':orden,
        'filter':filter,
        'total_ordenes':total_ordenes,
    }
    print(filter)
    return render(request , 'accounts/customer.html', context )

def crearOrden(request):
    form1 = OrdenForm()
    if request.method == 'POST':
        form1 = OrdenForm(request.POST)
        if form1.is_valid:
            form1.save()
            return redirect('home')

    
    return render(request , 'accounts/crear.html', {'form1':form1})


def crearCustomer(request):
    form = CustomerForm()
    if request.method == 'POST':
        form = CustomerForm(request.POST)
        if form.is_valid:
            form.save()
            return redirect('home')

    context = { 'form': form }



    return render(request , 'accounts/crear.html', context)




def eliminarOrden(request, pk):
    orden = Orden.objects.get(id=pk)
    if request.method == 'POST':
        orden.delete()
        return redirect('home')

    context = {'orden':orden}

    return render(request , 'accounts/eliminarOrden.html',context)


# def _UpdateORden ----> voy a necesitar el parametro id ,
# y pasarlo como instancia( HACERLO LUEGO)
def updateCostumer(request, id):

    customer = Customer.objects.get(id= id)
    form = CustomerForm(instance= customer)
    if request.method == 'POST':

        form = CustomerForm(request.POST, form)

        if request.is_valid:
            customer.save()


    context = { 'customer':customer,
                'form':form,
              }


    return render(request, 'accounts/Update.html',context)


def updateOrden(request, id):

    orden = Orden.objects.get(id= id)
    form = OrdenForm(instance= orden)
    if request.method == 'POST':

        form = OrdenForm(request.POST, instace=form)
        if request.is_valid:
            orden.save()


    context = {
                'form':form,
              }


    return render(request, 'accounts/Update.html',context)



def signin(request):

    if request.user.is_authenticated:
        return redirect('home')
    else:
        if request.method  =='POST': 
        
            username = request.POST.get('username')
            password = request.POST.get('password')
            user  = authenticate(request , username =username , password = password)
            if user is not None:
                login(request, user)
                return redirect('home')
            else: 
                messages.info(request, 'username is incorrect')


    context =  {
       

    }
    return render(request, 'accounts/signin.html', context)
 


def register(request): 
    
    if request.user.is_authenticated:
        return redirect('home')
    else: 
        form = CreateUserForm()
        if request.method == 'POST':
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request, 'profile was created' + user )
                return redirect('signin')

    context = {'form':form}
    return render (request, 'accounts/register.html', context)


def logoutUser(request):
    logout(request)
    return redirect('signin')





def crud(request):
    productos = Producto.objects.all()
    customers = Customer.objects.all()

    context = {
        'productos':productos,
        'customers':customers,

    }
    return render(request, 'accounts/crud.html', context)
    
def crear_producto(request):
    nombre1 = request.GET.get('nombre', None)
    descripcion1 = request.GET.get('descripcion', None)
    precio1 = request.GET.get('precio', None)
    categoria1 = request.GET.get('categoria', None)


    obj = Producto.objects.create(
        nombre= nombre1,
        descripcion= descripcion1,
        precio=precio1,
        categoria = categoria1
    )

    return JsonResponse(json.loads(serializers.serialize('json',[obj,])),safe=False)




def boton(request):
    text = request.GET.get('button_text')
    if request.is_ajax():
        t= time()
        return JsonResponse({'seconds':t}, status=200)

    return render(request, 'accounts/crud')