from django.forms import ModelForm, fields
from django.forms.models import fields_for_model
from .models import Customer, Orden, Producto
from django import forms 
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User



class OrdenForm(ModelForm):
    class Meta:
        model = Orden
        fields = '__all__'


class CustomerForm(ModelForm):


    class Meta:
        model = Customer
        fields = '__all__'

class ProductoForm(ModelForm):
    class Meta:
        model = Producto
        fields = '__all__'



class CreateUserForm(UserCreationForm):
    class Meta: 
        model = User
        fields = ['username', 'email', 'password1' , 'password2']