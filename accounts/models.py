from django.db import models


# Create your models here.




class Customer(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100, null=True)
    telefono = models.CharField(max_length=100, null=True)
    email = models.EmailField(max_length=100, null=True)
    created = models.DateTimeField(auto_now_add=True , null=True)


    def __str__(self):
        return self.nombre





class Producto(models.Model):



    nombre = models.CharField(max_length=100, null=True)
    precio = models.FloatField(null=True)
    categoria = models.CharField(max_length=100 ,null=True)
    descripcion = models.CharField(max_length=100, null=False)
    created = models.DateTimeField(auto_now_add=True, null =True)

    def __str__(self):
        return self.nombre


class Orden(models.Model):

    STATUS = (
        ('pendiente', 'pendiente'), 
        ('progreso', 'progreso'),
        ('entregado', 'entregado')

    )
    id = models.AutoField(primary_key=True)
    customer = models.ForeignKey(Customer, null=True, on_delete=models.SET_NULL)
    producto = models.ForeignKey(Producto, null=True, on_delete=models.SET_NULL)
    created =  models.DateTimeField(auto_now_add=True, null =True)
    status =  models.CharField(max_length=100, null=True, choices=STATUS)

    def __str__(self):
        return self.producto.nombre


